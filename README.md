# Docker Tutorial

Practicing and learning Docker. Join my progress with new files updated regularly.

## Usage

You need [docker](https://www.docker.com/get-started) to install this image.

```bash
docker build -t shehzaan/nginx .
docker run -p 8000:80 shehzaan/nginx
```


## License
[MIT](https://choosealicense.com/licenses/mit/)