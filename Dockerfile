FROM debian

## Installing dependecies 
RUN apt-get update && apt-get install -y \
	curl \
	gcc \
	libpcre3-dev \
	libssl-dev \
	make

RUN apt-get install --reinstall zlibc zlib1g zlib1g-dev

## Fetching the NGINX server
RUN curl http://nginx.org/download/nginx-1.16.0.tar.gz > /opt/nginx-1.16.0.tar.gz

WORKDIR /opt

## Extracting the .tar file
RUN tar xzf nginx-1.16.0.tar.gz

WORKDIR /opt/nginx-1.16.0

RUN ./configure

RUN make

RUN make install

RUN ln -sf /dev/stdout /usr/local/nginx/logs/access.log \
	&& ln -sf /dev/stderr /usr/local/nginx/logs/error.log

## Exposing port 80 on the Docker for testing
EXPOSE 80

CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]

